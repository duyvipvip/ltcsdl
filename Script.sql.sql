﻿CREATE DATABASE Coffee_Shop_Management_Software
GO

USE Coffee_Shop_Management_Software
GO

-- Food
-- Table
-- FoodCategory
-- Account
-- Bill
-- BillInfo

create TABLE TableFood 

	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL DEFAULT N'Table hasnt existed name',
	status NVARCHAR(100) NOT NULL DEFAULT N'Empty'	-- empty|| having people 

GO

alter table tablefood add constraint resetdefault default('Unknown-Name') for name

select * from tablefood
go
ALTER TABLE tablefood
   ALTER COLUMN name DROP DEFAULT
   ALTER TABLE tablefood modify HOCPHI DECIMAL (18, 2) DEFAULT 3500000;


--ALTER TABLE TableFood 
--MODIFY COLUMN status NVARCHAR(100) NOT NULL DEFAULT N'Being empty'

--go


CREATE TABLE Account
(
	UserName NVARCHAR(100) PRIMARY KEY,	
	DisplayName NVARCHAR(100) NOT NULL DEFAULT N'Avral',
	PassWord NVARCHAR(1000) NOT NULL DEFAULT 0,
	Type INT NOT NULL  DEFAULT 0 -- 1: admin && 0: staff
)
GO

CREATE TABLE FoodCategory
(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL DEFAULT N'Name havent set name'
)
GO
CREATE TABLE Food
(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL DEFAULT N'Name havent set name',
	idCategory INT NOT NULL,
	price FLOAT NOT NULL DEFAULT 0
	
	FOREIGN KEY (idCategory) REFERENCES dbo.FoodCategory(id)
)
GO

CREATE TABLE Bill
(
	id INT IDENTITY PRIMARY KEY,
	DateCheckIn DATE NOT NULL DEFAULT GETDATE(),
	DateCheckOut DATE,
	idTable INT NOT NULL,
	statuses int NOT NULL DEFAULT 0 -- 1: paid && 0: havent paid
	
	FOREIGN KEY (idTable) REFERENCES dbo.TableFood(id)
)
GO
--drop table Bill
--ALTER TABLE dbo.Bill DROP COLUMN s
--ALTER TABLE Bill
--add CONSTRAINTFK Bill BillInfor
--FOREIGN KEY(idBill)

--REFERENCES BillInfor(idBill)

CREATE TABLE BillInfo
(
	id INT IDENTITY PRIMARY KEY,
	idBill INT NOT NULL,
	idFood INT NOT NULL,
	count INT NOT NULL DEFAULT 0
	
	FOREIGN KEY (idBill) REFERENCES dbo.Bill(id),
	FOREIGN KEY (idFood) REFERENCES dbo.Food(id)
)
GO

insert into dbo.Account(
	UserName ,
	DisplayName, 
	PassWord ,
	Type 
)
values(
N'K9',
N'Rong K9',
N'1',
1
)
go
insert into dbo.Account(
	UserName ,
	DisplayName, 
	PassWord ,
	Type 
)
values(
N'Staff',
N'Staff',
N'1',
0
)

--select * from Account
go
create procedure Avral_GetaccountbyUsername 
@Username NVARCHAR(100)
as
begin
select * from Account where @UserName=dbo.Account.UserName
end
go

execute Avral_GetaccountbyUsername N'K9'

select * from Account where Username=N'Caogiachuc' and PassWord=N'123'

declare @i int =0
while @i<20
begin
insert dbo.TableFood(name)values(N'Table '+ cast(@i as  NVARCHAR(100)))
set @i=@i+1
end
go
declare @i int =0
while @i<20
begin
update dbo.TableFood set name=N'No.'+ cast(@i as  NVARCHAR(100)),@i=@i+1
end

update dbo.Tablefood set name=N'Table '+ cast(name as  NVARCHAR(100))
select * from tablefood
delete 


-- the second ways to add datas
insert dbo.TableFood(name,status)values(N'Table 1')
insert dbo.TableFood(name,status)values(N'Table 2')
insert dbo.TableFood(name,status)values(N'Table 3')


--creating procedure 
go 
create procedure USP_Getdatatable
as select * from TableFood
go

execute USP_Getdatatable
select * from tablefood


select * from TableFood

update dbo.TableFood set  status = N'Existed' where dbo.TableFood.id=1
go
-- inserting category of food 
insert FoodCategory (name)values(N'SeaFood')--55
insert FoodCategory (name)values(N'Farmfood')--56
insert FoodCategory (name)values(N'HomeFood')--57
--insert FoodCategory (name)values(N'StreetFood')--4
insert FoodCategory (name)values(N'FrogFood')--58
insert FoodCategory (name)values(N'FishFood')--59
insert FoodCategory (name)values(N'CowFood')--60
--insert FoodCategory (name)values(N'BuffalowFood')--8
insert FoodCategory (name)values(N'FruitFood')--61
insert FoodCategory (name)values(N'DrinkingFood')--62


go
--inserting name of food 
insert Food (name ,idCategory,price)values(N'Sá sùng',55,4000000)
insert Food (name ,idCategory,price)values(N'Bào ngư',55,1200000)
insert Food (name ,idCategory,price)values(N'Cua huỳnh đế',55,1000000)
insert Food (name ,idCategory,price)values(N'Tôm hùm',55,1200000)
insert Food (name ,idCategory,price)values(N'Tôm mũ ni đỏ',55,2000000)

insert Food (name ,idCategory,price)values(N'CÁ BỐNG ĐỤC KHO LÁ CHÈ XANH',56,800000)
insert Food (name ,idCategory,price)values(N'BÒ XÀO BÓNG HÀNH',56,500000)

insert Food (name ,idCategory,price)values(N'LÒNG HEO XÀO DỨA (THƠM)',57,400)
insert Food (name ,idCategory,price)values(N'THỊT XIÊN NƯỚNG',57,200000)
insert Food (name ,idCategory,price)values(N'Sườn nướng giấy bạc thơm ngon khó cưỡng',57,500000)

insert Food (name ,idCategory,price)values(N'Ếch chiên bơ',58,90000)
insert Food (name ,idCategory,price)values(N'Ếch chiên mè',58,50000)
insert Food (name ,idCategory,price)values(N'Ếch xào hành tỏi',58,30000)
insert Food (name ,idCategory,price)values(N'Ếch xào tía tô',58,35000)

insert Food (name ,idCategory,price)values(N'Ngọt mềm thịt bò nướng kiểu Hàn Quốc',60,90000)
insert Food (name ,idCategory,price)values(N' Gỏi sấu, thịt bò',60,50000)
insert Food (name ,idCategory,price)values(N'Xuýt xoa thịt bò nấu dưa chua',60,30000)
insert Food (name ,idCategory,price)values(N'Súp thịt bò rau củ ngon lành cho bữa sáng',60,35000)

insert Food (name ,idCategory,price)values(N'Cá kho dưa',59,20000)
insert Food (name ,idCategory,price)values(N'Cá hồi nướng tiêu chanh',59,15000)
insert Food (name ,idCategory,price)values(N'Ruốc cá hồi',59,25000)
insert Food (name ,idCategory,price)values(N'Chả cá, đậu phụ rán giòn',59,35000)

insert Food (name ,idCategory,price)values(N'Soda',62,15000)
insert Food (name ,idCategory,price)values(N'choka',62,20000)
insert Food (name ,idCategory,price)values('Coffee',62,15000)
insert Food (name ,idCategory,price)values(N'Sting',62,12000)
insert Food (name ,idCategory,price)values(N'Trà Đào',61,11000)
insert Food (name ,idCategory,price)values(N'bubble tea',61,16000)

select * from food
select * from FoodCategory
delete food
go
-- inserting bill table 
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses)values(GETDATE(),NULL,1,0);
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses)values(GETDATE(),NULL,2,0);
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses)values(GETDATE(),GETDATE(),3,1);
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses)values(GETDATE(),NULL,6,0);
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses)values(GETDATE(),NULL,7,1);
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses)values(GETDATE(),GETDATE(),9,0);



go
--inserting billinfor
insert BillInfo (idBill,idFood,count)values(1 ,1,3)
insert BillInfo (idBill,idFood,count)values(2 ,2,5)
insert BillInfo (idBill,idFood,count)values(3 ,3,6)
insert BillInfo (idBill,idFood,count)values(7 ,4,7)
insert BillInfo (idBill,idFood,count)values(8 ,5,8)


select * from FoodCategory
select * from BillInfo
select * from Bill

select * from TableFood
select name from Food where idCategory=1
select * from TableFood ,Bill where bill.idTable=Bill.idTable and Bill.statuses=1
select * from Food


select * from dbo.Bill where dbo.Bill.idTable=1 and dbo.Bill.status=0

create procedure updateexist
as

Update TableFood set status = N'existed'
from TableFood t1 inner join Bill t2 on t1.id = t2.idTable 

 go
 create procedure GetuncheckIDbyTableID
 @Idtable int 
 as
begin
select * from dbo.Bill where dbo.Bill.idTable=@Idtable 
 end

 execute GetuncheckIDbyTableID 1


select * from dbo.BillInfo where dbo.BillInfo.idBill=3

select * from Bill

drop table FoodCategory
drop table Food
drop table TableFood
drop table BillInfo
drop table Bill

select Food.name ,Food.price,BillInfo.count, Food.price*BillInfo.count as sum from BillInfo, bill, food where food.id = BillInfo.idFood and BillInfo.idBill = Bill.id and Bill.idTable =1 and bill.statuses=0
        
		go
select * from Food where Food.idCategory=1
go
alter procedure Insert_bill
@idTable int
as 
begin
insert Bill (DateCheckIn,DateCheckOut,idTable,statuses,Discount)values(GETDATE(),NULL,@idTable,0,0)
end 
go

execute Insert_bill 8
go
alter procedure Insert_billinfor
@idbill int , @idfood int , @count int
as 
begin
	declare @isexistbillinfor int 
	declare @foodcount int =1

	select @isexistbillinfor=id, @foodcount=b.count 
	from dbo.BillInfo as b 
	where idBill=@idbill and idFood=@idfood
		if(@isexistbillinfor>0)
			begin
				declare @newcount int = @foodcount+@count
				if(@newcount>0)
				update dbo.BillInfo set count =@foodcount+@count where idfood=@idfood
				else
				delete dbo.BillInfo where idBill=@idbill and idFood=@idfood 
			end
		else
			begin
			insert BillInfo (idBill,idFood,count)
			values(@idbill ,@idfood,@count)
			end
end




go
execute Insert_billinfor 1, 2, 3
execute Insert_bill  23
 
select MAX(id) from Bill
select id from Bill

go
--update table exist when we table doesn't exest in bill and billinfor  (the first way )
go
alter procedure UpdatestatusTable
as
	begin
		update TableFood set status=N'Existed'
		WHERE EXISTS (SELECT TableFood.id, Bill.idTable FROM Bill,BillInfo ,Food WHERE Bill.idTable=TableFood.id and bill.id=BillInfo.idBill and BillInfo.idFood=Food.id)
		update TableFood set status=N'Empty'
		WHERE NOT EXISTS (SELECT TableFood.id, Bill.idTable FROM Bill ,BillInfo, Food where Bill.idTable=TableFood.id and bill.id=BillInfo.idBill and BillInfo.idFood=Food.id)

	end

select * from TableFood
execute UpdatestatusTable
-
--using trgger in order to do this task specially billinfor table 
go



create trigger UpdateforBillinfo
ON BillInfo for insert,update
-- "ON" which one of table you want task
-- "FOR" what case for? for instance insert and update
as
begin
 declare @idbill int 
 select @idbill=idbill from inserted

 declare @idtable int 
 select @idtable=idtable from dbo.Bill where id=@idbill and statuses=0

 update dbo.TableFood set status=N'Existed' where id=@idtable
end  
go

-- creating the second trigger for updatebill
alter trigger Updatebill
on dbo.Bill for update
as 
begin
declare @idbill int 
select @idbill=id from inserted 
declare @idtable int
select @idtable =idtable from dbo.Bill where id=@idbill
declare @count int =0
select @count=COUNT(*) from dbo.Bill where idTable=@idtable and statuses=0
if(@count=0)
update dbo.TableFood set status =N'Empty' where id=@idtable;
end
go

delete BillInfo
go
delete Bill

SELECT * FROM TABLEFOOD

create database Rubbishcan
go 
use  Rubbishcan

create table main
(
Id int primary key, 
Name Nvarchar(200),
Addresses Nvarchar(200)
)

insert main(Id,Name,Addresses)values(1,N'Le van chuc',N'Dong Nai')
insert main(Id,Name,Addresses)values(2,N'Le Van Long' ,N' Dong Nai')
insert main(Id,Name,Addresses)values(3,N'Le Thi Thuy', N'Dong Nai')
insert main(Id,Name,Addresses)values(4,N'Le Thi Chinh',N'Dong Nai')
insert main(Id,Name,Addresses)values(5,N'Le Thi Chinh',N'Dong Nai')
insert main(Id,Name,Addresses)values(6,N'Le Van Thanh',N'Dong Nai')


create table child
(
Id int primary key, 
Idmain int,
Name Nvarchar(200),
Addresses Nvarchar(200)

FOREIGN KEY (Idmain) REFERENCES main(id)
)

insert child(Id,Idmain,Name,Addresses)values(1,1,N'Le van chuc1',N'Dong Nai')
insert child(Id,Idmain,Name,Addresses)values(2,2,N'Le Van Long1' ,N' Dong Nai')
insert child(Id,Idmain,Name,Addresses)values(3,3,N'Le Thi Thuy1', N'Dong Nai')
insert child(Id,Idmain,Name,Addresses)values(4,4,N'Le Thi Chinh1',N'Dong Nai')
insert child(Id,Idmain,Name,Addresses)values(5,5,N'Le Thi Chinh1',N'Dong Nai')
insert child(Id,Idmain,Name,Addresses)values(6,6,N'Le Van Thanh1',N'Dong Nai')


create trigger Trying
on 
go 
select * from dbo.tablefood

go
alter table Bill add Discount int default 0 
select * from bill
update bill set Discount=0



use coffe
go
Delete dbo.BillInfo 
go 
delete bill
alter table bill add grosslycost float 
go

alter procedure profitbyday
@daycheckin date,@daycheckout date
as 
begin
	select a.name, a.id as N'key-table', datecheckin,datecheckout, discount, sum(b.Grosslycost) as Grosslycost
	from tablefood as a ,bill as b 
	where b.datecheckin>=@daycheckin and b.datecheckout<=@daycheckout and b.statuses=1  
	and a.id=b.idtable
	group by a.name,a.id,datecheckin,datecheckout, discount
end
go


execute profitbyday '2018-10-10', '2018-10-10'

select * from food




select * from bill
select * from billinfo
select * from food
select * from tablefood
select * from foodcategory
select * from account
select * from account 
go
create Procedure Updateaccount
@username nvarchar(100), @displayname nvarchar(100),@password nvarchar(100),@newpassword nvarchar(100)
as 
begin

declare @isrightpass int =0
select @isrightpass=count(*) from Account where Account.UserName=@username and Account.PassWord=@password 
 if(@isrightpass=1)
 begin
		if(@newpassword=null or @newpassword='')
		begin 
		update dbo.Account set DisPlayName=@displayname where UserName=@username
		end  
		else 
		update dbo.Account set DisPlayName=@displayname, PassWord=@newpassword where UserName=@username

 end 

end 
go 
select * from food
select * from FoodCategory where id=21
update food set name=N'',idCategory=2,price=2 where id=4

select * from billinfo
select * from food

go
create trigger deletebillinfo
on billinfo for delete
as 
begin
 declare @idbillinfo int
 declare @idbill int 
 select @idbillinfo=id,@idbill=deleted.idbill from  deleted

declare @idtable int 
select @idtable=idtable from bill where id=@idbill

--we need to bring out the count 
declare @count int=0
select @count=COUNT(*) from billinfo as bi,bill as b where b.id=bi.idbill and b.id=@idbill and statuses=0

if(@count=0)
update tablefood set status=N'Empty' where id=@idtable
end 
go

-- function for seeking VietNamese or English or anyelse
CREATE FUNCTION [dbo].[fuConvertToUnsign1] ( @strInput NVARCHAR(4000) ) RETURNS NVARCHAR(4000)
AS 
BEGIN 
IF @strInput IS NULL 
RETURN @strInput 
IF @strInput = '' 
RETURN @strInput 
DECLARE @RT NVARCHAR(4000) 
DECLARE @SIGN_CHARS NCHAR(136) 
DECLARE @UNSIGN_CHARS NCHAR (136) 
SET @SIGN_CHARS = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệế ìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵý ĂÂĐÊÔƠƯÀẢÃẠÁẰẲẴẶẮẦẨẪẬẤÈẺẼẸÉỀỂỄỆẾÌỈĨỊÍ ÒỎÕỌÓỒỔỖỘỐỜỞỠỢỚÙỦŨỤÚỪỬỮỰỨỲỶỸỴÝ' +NCHAR(272)+ NCHAR(208) 
SET @UNSIGN_CHARS = N'aadeoouaaaaaaaaaaaaaaaeeeeeeeeee iiiiiooooooooooooooouuuuuuuuuuyyyyy AADEOOUAAAAAAAAAAAAAAAEEEEEEEEEEIIIII OOOOOOOOOOOOOOOUUUUUUUUUUYYYYYDD' 
DECLARE @COUNTER int 
DECLARE @COUNTER1 int 
SET @COUNTER = 1 
WHILE (@COUNTER <=LEN(@strInput)) 
BEGIN SET @COUNTER1 = 1 
WHILE (@COUNTER1 <=LEN(@SIGN_CHARS)+1) 
BEGIN 
IF UNICODE(SUBSTRING(@SIGN_CHARS, @COUNTER1,1)) = UNICODE(SUBSTRING(@strInput,@COUNTER ,1) ) 
BEGIN 
IF @COUNTER=1 
SET @strInput = SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)-1) 
ELSE 
SET @strInput = SUBSTRING(@strInput, 1, @COUNTER-1) +SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)- @COUNTER) 
BREAK 
END 
SET @COUNTER1 = @COUNTER1 +1 
END SET @COUNTER = @COUNTER +1 
END SET @strInput = replace(@strInput,' ','-') 
RETURN @strInput 
END
 select * from Dbo.food where [dbo].[fuConvertToUnsign1](name) like N'%'+[dbo].[fuConvertToUnsign1](N'sa')+'%'
 select * from food
 select * from account
 



 select * from foodcategory

 update FoodCategory set name=N''

select * from tablefood
select * from bill
 select * from billinfo
 select * from food 
 select * from foodcategory
 update tablefood set status=N'Empty'

 select MAX(id) from Bill
select a.id as id,b.idtable as idtable,b.id as idbill,c.idbill as idbill 
from tablefood as a,bill as b,billinfo as c 
where a.id=b.idtable and b.id=c.idbill 



select * from tablefood 
select * from bill

select * from billinfo


delete BillInfo where BillInfo.idbill =209
go
delete Bill where Bill.idtable=41 and bill.id=209
go
update tablefood set name=N'No.1',status=N'Empty' where id =41

